-module(system_handler).
-compile(export_all).

-include("globals.hrl").

%% 推送消息给用户
%% [{user_id, 1}, {type, 2}, {message, "aaa"}]
%% type: 
%% 1. 有新的短消息
%% 2. 刷新余额
handle_info({push, Data, Session}) -> 
    UserId = websocket_handler:get_query(Data, user_id),
    case list_to_integer(websocket_handler:get_query(Data, type)) of 
            1 -> 
                    messages_changed(UserId, Data),
        {Session, #message{message = "消息发送成功"}};
            2 -> 
                    finance_changed(UserId, Data),
        {Session, #message{message = "资金有变动"}};
            _ -> 
        {Session, #message{code = ?LINE, message = "错误的消息类型"}}
    end;

%% 广播消息给所有用户
%% [{message, ""}]
handle_info({broadcast, _Message}) -> 
    %% 1. 提取现有的所有 socket
    %% 2. 给每个用户发送消息
    ok.

%% 消息变更, 提醒前台用户, 让前台去刷新消息数量
messages_changed(UserId, _Data) ->
    websocket_handler:push_user_message(UserId, #message{type = 1, state = 1, message = "您有新的短消息, 请注意查收"}).

%% 资金变更, 让前台刷新用户余额
finance_changed(UserId, _Data) -> 
    websocket_handler:push_user_message(UserId, #message{type = 2, state = 1, message = "您的账户有资金变动"}).
