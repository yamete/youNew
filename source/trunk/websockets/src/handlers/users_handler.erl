-module(users_handler).
-compile(export_all).

-include("globals.hrl").

%% 建立连接时的默认用户信息初始化
%% 如果是用户从前台建立的链接, 必须先调用此函数
handle_info({init, Data, Session}) -> 
    case websocket_handler:get_query(Data, user_id) of 
        [] -> 
            {Session, #message{code = ?LINE, message = "user_id error."}};
        UserId -> 
            case websocket_handler:get_query(Data, user_name) of 
                [] -> 
                    {Session, #message{code = ?LINE, message = "user name error."}};
                UserName -> 
                    NewSession = #session{user_id = UserId, user_name = UserName, socket = Session#session.socket, connected = [date(), time()]},
                    websocket_handler:cache_insert(NewSession),
                    {NewSession, #message{message = "initialized"}}
            end
    end.
