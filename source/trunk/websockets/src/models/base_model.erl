-module(base_model).
-compile(export_all).

-include("globals.hrl").

%% 启动进程
start() -> 
    start_mysql().

%% 开启数据库连接
start_mysql() -> 
    case lists:member(mysql, erlang:loaded()) of
        false -> 
            mysql:start_link(?DB_LINK, ?DB_HOST, ?DB_USER, ?DB_PASS, ?DB_NAME),
            mysql:connect(?DB_LINK, ?DB_HOST, ?DB_PORT, ?DB_USER, ?DB_PASS, ?DB_NAME, true), 
            {ok, started};
        _ -> {ok, has_started}
    end.