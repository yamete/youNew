-module(websocket_app).
-compile(export_all).

start() -> 
    io:format("~nStarting push server .... ", []),
    websocket_handler:start(),
    io:format(" OK.~n", []),
    websocket_server:start().