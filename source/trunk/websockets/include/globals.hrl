%% 常量信息定义

%% 监听端口
-define(PORT, 9345).

%% 标识ID
-define(DB_LINK, db_websocket).
%% 主机地址
-define(DB_HOST, "localhost").
%% 用户名称
-define(DB_USER, "cp_imf").
%% 用户密码
-define(DB_PASS, "cp_#_!159").
%% 数据库名
-define(DB_NAME, "cp_imf_money").

%% 用户会话
-record(session, {user_id, user_name, socket, connected}).

%% 用户消息
-record(message, {code = 0, type = 0, state = 0, message = ""}).
