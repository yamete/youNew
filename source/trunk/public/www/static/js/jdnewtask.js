$(document).ready(function(){
    $('.resa').click(function(){
        $('.rele').css('display','block');
    });
    $('.jinen').click(function(){
        $('.jine').css('display','block');
    });
    $('.extra').click(function(){
        $('.extran').css('display','block');
    });
    $('.chec').click(function(){
        $('.checn').css('display','block');
    });
    $('.false').click(function(){
        $('.rele').css('display','none');
        $('.jine').css('display','none');
        $('.extran').css('display','none');
        $('.checn').css('display','none');
        $(".expte").css("display","none");
    });
    $(".aui_close").click(function(){
        $(".expte").css("display","none");
        $('.checn').css('display','none');
        $('.jine').css('display','none');
        $('.extran').css('display','none');
    });
    $(".product-num").live("change",function(){
        getTotalPrice();
        getFee();
    });
    //保存并发布任务
    $("input#saveRelease").click(function(){
        $("#taskEditForm").ajaxSubmit({
            dataType:"json",
            data:{task_is_release:1,task_is_save:1},
            success:function(result){
                if(result.status==1){
                    location.href='/task/jdtask';
                }else{
                    str="<h2>"+result.msg+"</h2>";
                    $(".expte #proinfo").html(str);
                    $(".expte").css("display","block");
                }
            }
        });
    });
    //取消任务
    $("input#cancel").click(function(){
        location.href='/task/tasktemplate';
    });
    //手动上传商品缩略图
    $(".upload-img").live("change",function(){
        $this=$(this);
        var formData=new FormData();
        formData.append('file',$this[0].files[0]);
        $.ajax({
            url: '/ajax/uploadthumb',
            type: 'POST',
            cache: false,
            data: formData,
            dataType:'json',
            processData: false,
            contentType: false
        }).done(function(res) {
            if(res.status==1){
                $this.parent().parent().prev("td").find("img").attr("src",res.info);
                $this.parent().parent().prev("td").find("input").val(res.info);
            }else{
                alert(res.info);
            }
        }).fail(function(res) {
            alert('上传文件失败，请稍候重重试！');
        });
    });
    //自动上传商品缩略图
    $(".goods-link").live("change",function(){
        var $this=$(this);
        var img=$this.parent().parent().find(".product-img-val").val();
        if(img==''){
            var goodsLink=$(this).val();
            var shopId=$("#shopId").val();
            if(shopId=='0'){
                $(".expte #proinfo").html("<h2>请选择掌柜号！</h2>");
                $(".expte").css("display","block");
                return false;
            }
            $.ajax({
                url:"/ajax/getimg",
                type:"POST",
                data:{goodsLink:goodsLink,shopId:shopId},
                dataType:"json",
                success:function(result){
                    if(result.status==1){
                        $this.parent().parent().find("img").attr("src",result.msg);
                        $this.parent().parent().find(".product-img-val").val(result.msg);
                    }else{
                        str="<h2>"+result.msg+"</h2>";
                        $(".expte #proinfo").html(str);
                        $(".expte").css("display","block");
                    }
                }
            });
        }
    });
    $("#level").change(function(){
        getFee();
    });
    //计算商品总价格
    function getTotalPrice(){
        var totalPrice=0;
        $(".product-num").each(function(n){
            var num=parseFloat($(this).val());
            var price=parseFloat($(".product-price").eq(n).val());
            if(num>0&&price>0){
                totalPrice=totalPrice+num*price;
            }
        });
        totalPrice=totalPrice.toFixed(2);
        $("#totalPrice").html(totalPrice+"元");
    }
    $(".product-price").live("change",function(){
        getFee();
        getTotalPrice();
    });
    $("#addFee").change(function(){
        getFee();
    });
    $("input[name='task_entrance']").change(function(){
        getFee();
    });
    $("input[name='task_return_type']").change(function(){
        getFee();
    });
});
function addProduct(){
    var num=$(".product-row").length;
    if(num>=4){
        $(".expte #proinfo").html("<h2>最多只能添加3个商品！</h2>");
        $(".expte").css("display","block");
        return false;
    }
    tr="<tr class='td_c product-row'>";
    tr+="<td><input class='form-control goods-link' type='text' name='goodsLink[]'></td>";
    tr+="<td><input class='form-control product-price input-mini' type='text' name='goodsPrice[]'></td>";
    tr+="<td><input class='form-control product-num input-mini' type='text' name='goodsNum[]' value='1'></td>";
    tr+="<td><textarea class='form-control' name='searchPosition[]'></textarea></td>";
    tr+="<td><a href='javascript:void(0);'> <img class='product-img img-thumbnail' src='/static/images/noimage.gif' /></a>";
    tr+="<input class='product-img-val' name='goodsImg[]' type='hidden' /></td>";
    tr+="<td><span class='btn file-btn btn-mini'>上传图片<input class='upload-img' type='file' /></span><span class='btn btn-mini' onclick='delProduct($(this))'>删除商品</span></td>";
    tr+="</tr>";
    $('#goodsBaseBody').append(tr);
    getFee();
}
function delProduct(obj){
    var totalPrice=parseFloat($("#totalPrice").text());
    var price=parseFloat(obj.parent().parent().find(".product-price").val());
    var num=parseFloat(obj.parent().parent().find(".product-num").val());
    if(totalPrice>0&&price>0&&num>0){
        totalPrice=totalPrice-price*num;
        $("#totalPrice").html(totalPrice+"元");
    }
    obj.parent().parent().remove();
    getFee();
}
function getFee(){
    var priceStr="";
    var numStr="";
    var level=$("#level").val();
    var addFee=$("#addFee").val();
    var entrance=$("input[name='task_entrance']:checked").val();
    $(".product-price").each(function(){
        priceStr+=$(this).val()+",";
    });
    $(".product-num").each(function(){
        numStr+=$(this).val()+",";
    });
    var accountType=$("input[name='task_return_type']:checked").val();
    var goodsNum=$(".product-row").length;
    $.ajax({
        url:"/ajax/getfee",
        type:"POST",
        data:{level:level,addFee:addFee,entrance:entrance,priceStr:priceStr,numStr:numStr,accountType:accountType,goodsNum:goodsNum},
        dataType:"json",
        success:function(result){
            if(result.status==1){
                str="<h2>"+result.msg+"</h2>";
                $(".expte #proinfo").html(str);
                $(".expte").css("display","block");
            }else{
                $("#taskPrice").text(result.msg+"元");
            }
        }
    });
}