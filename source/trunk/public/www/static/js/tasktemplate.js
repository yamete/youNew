$(document).ready(function(){
    $(".expte .false").click(function(){
        $(".expte").css("display","none");
    });
    $(".expte .aui_close").click(function(){
        $(".expte").css("display","none");
    });
    $(".jine .aui_close").click(function(){
        $(".jine").css("display","none");
    });
    $(".jine .false .btn-orange").click(function(){
        $(".jine").css("display","none");
    });
    //更新库存
    $(".jine .false .update").click(function(){
        $("#taskNum").ajaxSubmit({
            dataType:"json",
            success:function(result){
                $(".jine").css("display","none");
                if(result.status==1){
                    location.href='/task/tasktemplate';
                }else{
                    $(".expte #proinfo").html("<h2>"+result.msg+"</h2>");
                    $(".expte").css("display","block");
                }
            }
        });
    });
});
function releaseTask(){
    var type=$("#type").val();
    if(type==0) {
        $(".expte #proinfo").html("<h2>请选择要新建的任务类型</h2>");
        $(".expte").css("display","block");
    }else{
        checkBind(type);
    }
}
function checkBind(v){
    $.ajax({
        url:"/ajax/checkbind",
        type:"POST",
        data:{v:v},
        dataType:"json",
        success:function(result){
            if(result.status==1){
                if(v==1){
                    location.href="/task/tbnewtask";
                }else if(v==2){
                    location.href="/task/jdnewtask";
                }
            }else{
                if(v==1){
                    location.href="/task/bindtb";
                }else if(v==2){
                    location.href="/task/bindjd";
                }
            }
        }
    });
}
function operate(id,v){
    $.ajax({
        url:"/ajax/templateoprate",
        type:"POST",
        data:{id:id,v:v},
        dataType:"json",
        success:function(result){
            if(result.status==1){
                location.href="/task/tasktemplate";
            }else{
                $(".expte #proinfo").html("<h2>"+result.msg+"</h2>");
                $(".expte").css("display","block");
            }
        }
    });
}
function deleteTemplate(id){
    $.ajax({
        url:"/ajax/deletetemplate",
        type:"POST",
        data:{id:id},
        dataType:"json",
        success:function(result){
            if(result.status==1){
                location.href="/task/tasktemplate";
            }else{
                $(".expte #proinfo").html("<h2>"+result.msg+"</h2>");
                $(".expte").css("display","block");
            }
        }
    });
}
function editNum(id){
    $("#taskId").val(id);
    $(".jine").css("display","block");
}
function editTemplate(id,type){
    if(type==1){
        location.href="/task/tbedittask/"+id;
    }else if(type==2){

    }
}