﻿$(function () {
    /*
    *思路大概是先为每一个required添加必填的标记，用each()方法来实现。
    *在each()方法中先是创建一个元素。然后通过append()方法将创建的元素加入到父元素后面。
    *这里面的this用的很精髓，每一次的this都对应着相应的input元素，然后获取相应的父元素。
    *然后为input元素添加失去焦点事件。然后进行用户名、邮件的验证。
    *这里用了一个判断is()，如果是用户名，做相应的处理，如果是邮件做相应的验证。
    *在jQuery框架中，也可以适当的穿插一写原汁原味的javascript代码。比如验证用户名中就有this.value，和this.value.length。对内容进行判断。
    *然后进行的是邮件的验证，貌似用到了正则表达式。
    *然后为input元素添加keyup事件与focus事件。就是在keyup时也要做一下验证，调用blur事件就行了。用triggerHandler()触发器，触发相应的事件。
    *最后提交表单时做统一验证
    *做好整体与细节的处理
    */
    //如果是必填的，则加红星标识.
    $("form :input.required").each(function () {
        var $required = $("<strong class='high'> *</strong>"); //创建元素
        $(this).parent().append($required); //然后将它追加到文档中
    });
    //文本框失去焦点后
    $('form :input').blur(function () {
        var $parent = $(this).parent();
        $parent.find(".formtips").remove();
        //验证用户名
        if ($(this).is('#userName')) {
            if (this.value == "" || this.value.length < 4) {
                var errorMsg = '请输入至少4位的用户名.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else if (!/^[\@A-Za-z0-9\_]{4,16}$/.test(this.value)) {
                var errorMsg = '用户名4-16字符和数字组成,（不包含特殊字符#,!,@...等）';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "/api/chkuser.ashx",
                    data: "username=" + this.value + "&act=ok",
                    async: false,
                    success: function (msg) {
                        //alert("Data Saved: " + msg);
                        if (msg == "suc") {
                            var okMsg = '√';
                            $parent.append('<span class="formtips onSuccess" style="color:#008000;">' + okMsg + '</span>');
                        } else {
                            var errorMsg = msg;
                            $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
                        }
                    }
                });

            }
        }
        //验证登陆密码
        if ($(this).is('#userPwd')) {
            if (this.value == "" || (this.value != "" && !/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,14}$/.test(this.value))) {
                var errorMsg = '请输入6-14的登陆密码,密码不能是纯数字和字母.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {
                var repPass1 = /[0-9]{1,}/; //数字
                var repPass2 = /[a-zA-Z]{1,}/; //字母 
                if (!repPass1.test(this.value) || !repPass2.test(this.value)) {
                    var errorMsg = '密码不能是纯数字和字母.';
                    $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
                } else if ($('#userName').val() == this.value)
                {
                    var errorMsg = '密码和用户名不能相同.';
                    $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
                }else                
                {
                    var okMsg = '√';
                    $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
                 }
            }
        }
        //验证确认登陆密码
        if ($(this).is('#qruserPwd')) {
            var userPwd=$('#userPwd').val();
            if (this.value == "" || (this.value != "" && !/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,14}$/.test(this.value))) {
                var errorMsg = '两次密码输入不一致，请重新输入！';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {

                if (this.value != userPwd) {
                    var errorMsgc = '两次密码输入不一致，请重新输入！';
                 $parent.append('<span class="formtips onError">' + errorMsgc + '</span>');
                }
                else
                {
                var okMsg = '√';
                $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
                }
                
            }     
        }


        //QQ验证
        if ($(this).is('#qq')) {
            if (this.value == "" || (this.value != "" && !/^[1-9][0-9]{4,9}$/.test(this.value))) {
                var errorMsg = '请输入正确的QQ号码.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {
                var okMsg = '√';
                $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
            }
        }
        //手机号码验证
        if ($(this).is('#tel')) {
            if (this.value == "" || (this.value != "" && !/^1[3,4,5,8,7]\d{9}$/.test(this.value))) {
                var errorMsg = '请输入正确的11位手机号码.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {
                //手机号码检查是否已被用过
                $.ajax({
                    type: "POST",
                    url: "/api/chkusertel.ashx",
                    data: "tel=" + this.value + "&act=ok",
                    async: false,
                    success: function (msg) {
                        //alert("Data Saved: " + msg);
                        if (msg == "suc") {
                            var okMsg = '√';
                            $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
                        } else {
                            var errorMsg = msg;
                            $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
                        }
                    }
                });
            }
        }

        //身份验证
        if ($(this).is('#userType')) {
            var _userype = $('input[name="userType"]:checked').val();
            $('#userType').val(_userype);
            if (this.value == "") {
                var errorMsg = '请选择身份.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {
                var okMsg = '√';
                $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
            }
        }
        //验证邮件
        if ($(this).is('#email')) {
            if (this.value == "" || (this.value != "" && !/.+@.+\.[a-zA-Z]{2,4}$/.test(this.value))) {
                var errorMsg = '请输入正确的E-Mail地址.';
                $parent.append('<span class="formtips onError">' + errorMsg + '</span>');
            } else {
                var okMsg = '√';
                $parent.append('<span class="formtips onSuccess" style="color:#008000;" >' + okMsg + '</span>');
            }
        }
    }).keyup(function () {
        //$(this).triggerHandler("blur");
    }).focus(function () {
        $(this).triggerHandler("blur");
    }); //end blur


    //提交，最终验证。
    $('#send').click(function () {
        $("form :input.required").trigger('blur');
        var numError = $('form .onError').length;
        if (numError) {
            return false;
        }
        //提交数据
        $("#send").attr("disabled", "disabled");
        $.ajax({
            cache: true,
            type: "POST",
            url: "/api/reguser.ashx",
            data: $('#regform').serialize(), // 你的formid
            async: false,
            error: function (request) {
                alert("注册失败!" + request.toString());
            },
            success: function (data) {
                if (data == 'erroryzm') {
                    alert('非法注册！');
                    $("#reg_code").val('');
                    $("#send").removeAttr("disabled");
                } else if (data == "error") {
                    //否则错误提示
                    alert('注册失败，换一个账号试一试！');
                    $("#userName").val('');
                    $("#send").removeAttr("disabled");
                } else {
                    //返回用户编号
                    //alert("注册成功！请联系客服审核账户！！！");
                    //location.href = "/ApprUser.aspx?code=" + data;
                    var fzsite = $("#sid").val();
                    //alert(fzsite);
                    if (fzsite == "0") {
                        //alert("下一步，请进行短信认证！");
                        window.location.href = "/reg/gotouid.aspx?code=" + data;


                    } else {
                        alert("账户已注册成功，请前去开通会员！！");
                        window.location.href = "/RegPay/";
                    }
                }
            }
        });
    });
})
