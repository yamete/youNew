$(document).ready(function(){
    $("#send").click(function(){
        $(this).attr("disabled","disabled");
        $("#regform").ajaxSubmit({
            url:"/ajax/doregister",
            dataType:"json",
            success:function(data){
                if(data.status==1){
                    alert("注册成功！");
                    location.href='/user';
                }else if(data.status==0){
                    alert("注册失败，请检查您的输入是否有误！");
                    $("#send").removeAttr("disabled");
                }
            }
        });
    });
});
function checkName(){
    var userName=$("#userName").val();
    $.ajax({
        type:"POST",
        url:"/ajax/checkname",
        dataType:"json",
        data:{userName:userName},
        success:function(result){
            $("#spanName").html(result.msg);
        }
    });
}
function checkPassword(){
    var userPwd=$("#userPwd").val();
    $.ajax({
        type:"POST",
        url:"/ajax/checkpassword",
        dataType:"json",
        data:{userPwd:userPwd},
        success:function(result){
            $("#spanPwd").html(result.msg);
        }
    });
}
function checkrePassword(){
    var rePwd=$("#rePwd").val();
    var userPwd=$("#userPwd").val();
    $.ajax({
        type:"POST",
        url:"/ajax/checkrepassword",
        dataType:"json",
        data:{rePwd:rePwd,userPwd:userPwd},
        success:function(result){
            $("#spanrePwd").html(result.msg);
        }
    });
}
function checkQQ(){
    var qq=$("#qq").val();
    $.ajax({
        type:"POST",
        url:"/ajax/checkqq",
        dataType:"json",
        data:{qq:qq},
        success:function(result){
            $("#spanQQ").html(result.msg);
        }
    });
}
function checkMobile(){
    var mobile=$("#mobile").val();
    $.ajax({
        type:"POST",
        url:"/ajax/checkmobile",
        dataType:"json",
        data:{mobile:mobile},
        success:function(result){
            $("#spanMobile").html(result.msg);
        }
    });
}
function chCaptcha(){
    var srcVal=$("#captcha").attr("src");
    $("#captcha").attr("src",srcVal+"?"+Math.random());
}
