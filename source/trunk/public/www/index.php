<?php
/**
 * 网站根目录
 */
define('WEB_ROOT', str_replace("\\","/",dirname(__FILE__)));

/**
 * app名称
 */
define('APP_NAME', 'frontend');

/**
 * app路径
 */
define('APP_DIR', realpath('../../apps').'/'.APP_NAME); //app路径

/**
 * 配置文件
 */
require APP_DIR . '/config/conf.php';

/**
 * 运行主程序
 */
Conf::runMvc();