$(document).ready(function(){
    $("#allID").on("ifChecked",function(){
        $("input[name='delID']").iCheck("check").attr("checked",true);
    });
    $("#allID").on("ifUnchecked",function(){
        $("input[name='delID']").iCheck("uncheck").removeAttr("checked");
    });
});
function look(id){
    $.ajax({
        url:"/user/look",
        type:"POST",
        data:{id:id},
        success:function(result){
            $("#myModal .modal-body").html(result);
            $('#myModal').modal();
        }
    });
}
function edit(id){
    $.ajax({
        url:"/user/edit",
        type:"POST",
        data:{id:id},
        success:function(result){
            $("#myModal1 .table").html(result);
            $('#myModal1').modal();
        }
    });
}
function doedit(){
    $("#editForm").ajaxSubmit({
        dataType:"json",
        success:function(data){
            if(data.status==1){
                $('#myModal').on('hidden.bs.modal', function (e) {
                    location.href="/user";
                });
                $("#myModal .modal-body").html("编辑成功！");
            }else{
                $("#myModal .modal-body").html("编辑失败！");
            }
            $("#myModal").modal();
        }
    });
    $('#myModal1').modal('hide');
}
function del(id){
    var boolVar=window.confirm("确定要删除吗？");
    if(boolVar){
        $.ajax({
            url:"/user/del",
            type:"POST",
            data:{ids:id},
            dataType:"json",
            success:function(result){
                if(result.status==1){
                    $('#myModal').on('hidden.bs.modal', function (e) {
                        location.href="/user";
                    });
                    $("#myModal .modal-body").html("删除成功！");
                }else{
                    $("#myModal .modal-body").html("删除失败！");
                }
                $('#myModal').modal();
            }
        });
    }
}
function dels(){
    var boolVar=window.confirm("确定要删除吗？");
    if(boolVar){
        var objNum=$("input[name='delID']:checked").length;
        if(objNum<1){
            $("#myModal .modal-body").html("请至少选择一项！");
            $('#myModal').modal();
            return false;
        }
        var str="";
        $("input[name='delID']:checked").each(function(){
            str+=$(this).val()+",";
        });
        $.ajax({
            url:"/user/del",
            type:"POST",
            data:{ids:str},
            dataType:"json",
            success:function(result){
                if(result.status==1){
                    $('#myModal').on('hidden.bs.modal', function (e) {
                        location.href="/user";
                    });
                    $("#myModal .modal-body").html("删除成功！");
                }else{
                    $("#myModal .modal-body").html("删除失败！");
                }
                $('#myModal').modal();
            }
        });
    }
}