function add(){
    $("#addForm").ajaxSubmit({
        dataType:"json",
        success:function(data){
            if(data.status==1){
                $('#myModal').on('hidden.bs.modal', function (e) {
                    location.href="/administrator";
                });
                $("#myModal .modal-body").html("添加成功！");
            }else{
                $("#myModal .modal-body").html("添加失败，请检查输入！");
            }
            $('#myModal').modal();
        }
    });
}