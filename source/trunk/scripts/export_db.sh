#!/bin/bash

/usr/local/pgsql/bin/pg_dump -h localhost -U postgres jc_master > ./db.sql
tar czf db.tar.gz ./db.sql
rm -rf db.sql
