<?php
use Phalcon\Mvc\Controller,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class UserController extends Controller{
    public function indexAction(){
        $pageOffset=5;//页码偏移量
        $limit=3;//每页显示记录数
        $currentPage=(int)@$_GET["page"]<1?1:@$_GET["page"];
        $timeArr=array();
        $phql="select u.* from User u order by u.user_reg_time desc";
        $recordList=$this->modelsManager->executeQuery($phql);
        $paginator=new PaginatorModel(
            array(
                "data"  => $recordList,
                "limit" => $limit,
                "page"  => $currentPage
            )
        );
        $page=$paginator->getPaginate();
        foreach($page->items as $item){
            $timeArr[$item->user_id]=date("Y-m-d H:i",$item->user_reg_time);
        }
        if($page->last<=$pageOffset*2+1){  //若要总页数小于或等于要显示的页码数，让起始页码等于1，结束页码等于总页数
            $startnum=1;
            $lastnum=$page->last;
        }else{  //若总页数大于要显示的页码数
            if($currentPage-$pageOffset<=1){   //若当前页码减去偏移量小于或等于1，让起始页码等于1，结束页码等于要显示的页码数
                $startnum=1;
                $lastnum=$pageOffset*2+1;
            }else{  //若当面页码减去偏移量大于1
                $startnum=$currentPage-$pageOffset <= 1 ? 1 : $currentPage-$pageOffset;   //若当前页码减去偏移量小于或等于1，则让起始页码等于1，否则就让起始页码等于当前页码减去偏移量
                $startnum=$currentPage+$pageOffset >= $page->last ? $page->last-$pageOffset*2 : $currentPage-$pageOffset; //若当前页码加上偏移量大于或等于总页数，则让起始页码等于总页数减去偏移量的2倍，否则就让起始页码等于当前页码减去偏移量
                $lastnum=$currentPage+$pageOffset >= $page->last ? $page->last:$currentPage+$pageOffset;  //若当前页码加上偏移量大于或等于总页数，则让结束页码等于总页数，否则就让结束页码等于当前页码加上偏移量
            }
        }
        $this->view->setVars(array(
            'page'=>$page,
            'timeArr'=>$timeArr,
            'startNum'=>$startnum,
            'lastNum'=>$lastnum,
            'currentPage'=>$currentPage,
            'cat1'=>'user',
            'cat2'=>'userList'
        ));
    }
    public function addAction(){
        $this->view->setVars(array(
            'cat1'=>'user',
            'cat2'=>'userAdd'
        ));
    }
    public function doaddAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                $data['time']=time();
                $data['type']=2;
                $data['pwd']=sha1($data['pwd']);
                $phql="insert into User "
                    ."(user_name,user_pwd,user_qq,user_mobile,user_reg_time,user_identity,user_type) values "
                    ."(:name:,:pwd:,:qq:,:mobile:,:time:,:identity:,:type:)";
                $result=$this->modelsManager->executeQuery($phql,$data);
                if($result->success()){
                    echo json_encode(array("status"=>1));
                    exit;
                }else{
                    echo json_encode(array("status"=>0));
                    exit;
                }
            }
        }
    }
    public function lookAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data = $this->request->getPost();
                $phql="select u.* from User u where u.user_id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                $str="<table class='table'>";
                foreach($record as $r){
                    if($r->user_identity==1){
                        $identity='刷手';
                    }else{
                        $identity='商家';
                    }
                    if($r->user_type==1){
                        $type='外部';
                    }else{
                        $type='内部';
                    }
                    $str.="<tr><td>会员名：</td><td>".$r->user_name."</td></tr>";
                    $str.="<tr><td>身份：</td><td>".$identity."</td></tr>";
                    $str.="<tr><td>类型：</td><td>".$type."</td></tr>";
                    $str.="<tr><td>余额：</td><td>".$r->user_money."</td></tr>";
                    if($r->user_identity==1){
                        $str.="<tr><td>保证金：</td><td>".$r->user_deposit."</td></tr>";
                        $str.="<tr><td>被投诉数：</td><td>".$r->user_complain."</td></tr>";
                        $str.="<tr><td>投诉撤销卡数量：</td><td>".$r->user_cancel_complain."</td></tr>";
                    }else{
                        $str.="<tr><td>发布点：</td><td>".$r->user_release_point."</td></tr>";
                        $str.="<tr><td>冻结金额：</td><td>".$r->user_freeze_money."</td></tr>";
                    }
                    $str.="<tr><td>注册时间：</td><td>".date("Y-m-d H:i",$r->user_reg_time)."</td></tr>";
                }
                $str.="</table>";
                echo $str;
                exit;
            }
        }
    }
    public function editAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data = $this->request->getPost();
                $phql="select u.* from User u where u.user_id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                foreach($record as $r){
                    if($r->user_identity==1){
                        $identity='刷手';
                    }else{
                        $identity='商家';
                    }
                    if($r->user_type==1){
                        $type='外部';
                    }else{
                        $type='内部';
                    }
                    $str.="<tr><td>会员名：</td><td>".$r->user_name."</td></tr>";
                    $str.="<tr><td>密码：</td><td><input type='password' class='form-control' name='pwd' placeholder='不修改密码请留空'></td></tr>";
                    $str.="<tr><td>身份：</td><td>".$identity."</td></tr>";
                    $str.="<tr><td>类型：</td><td>".$type."</td></tr>";
                    $str.="<tr><td>余额：</td><td><input type='text' class='form-control' name='money' value='".$r->user_money."'></td></tr>";
                    $str.="<tr><td>经验：</td><td><input type='text' class='form-control' name='exp' value='".$r->user_exp."'></td></tr>";
                    $str.="<tr><td>积分：</td><td><input type='text' class='form-control' name='point' value='".$r->user_point."'></td></tr>";
                    if($r->user_identity==1){
                        $str.="<tr><td>保证金：</td><td><input type='text' class='form-control' name='deposit' value='".$r->user_deposit."'></td></tr>";
                        $str.="<tr><td>被投诉数：</td><td>".$r->user_complain."</td></tr>";
                        $str.="<tr><td>投诉撤销卡数量：</td><td><input type='text' class='form-control' name='cancel_complain' value='".$r->user_cancel_complain."'></td></tr>";
                    }else{
                        $str.="<tr><td>发布点：</td><td><input type='text' class='form-control' name='release_point' value='".$r->user_release_point."'></td></tr>";
                        $str.="<tr><td>冻结金额：</td><td><input type='text' class='form-control' name='freeze_money' value='".$r->user_freeze_money."'></td></tr>";
                    }
                    $str.="<tr><td>注册时间：</td><td>".date("Y-m-d H:i",$r->user_reg_time)."</td></tr>";
                }
                $str.="<input type='hidden' name='id' value='".$data['id']."'>";
                echo $str;
                exit;
            }
        }
    }
    public function doeditAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                $record=User::findFirst($data['id']);
                if(empty($data['pwd'])){
                    if($record->user_identity==1){
                        $phql="update User set user_point=:point:,user_exp=:exp:,user_money=:money:,user_deposit=:deposit:,user_cancel_complain=:cancel_complain: where user_id=:id:";
                    }else{
                        $phql="update User set user_point=:point:,user_exp=:exp:,user_money=:money:,user_release_point=:release_point:,user_freeze_money=:freeze_money: where user_id=:id:";
                    }
                    unset($data['pwd']);
                }else{
                    $data['pwd']=sha1($data['pwd']);
                    if($record->user_identity==1){
                        $phql="update User set user_point=:point:,user_exp=:exp:,user_pwd=:pwd:,user_money=:money:,user_deposit=:deposit:,user_cancel_complain=:cancel_complain: where user_id=:id:";
                    }else{
                        $phql="update User set user_point=:point:,user_exp=:exp:,user_pwd=:pwd:,user_money=:money:,user_release_point=:release_point:,user_freeze_money=:freeze_money: where user_id=:id:";
                    }
                }
                $this->modelsManager->executeQuery($phql,$data);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
    public function delAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()) {
                $data = $this->request->getPost();
                $str = rtrim($data['ids'], ',');
                $phql = "delete from User where user_id in (" . $str . ")";
                $this->modelsManager->executeQuery($phql);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
}