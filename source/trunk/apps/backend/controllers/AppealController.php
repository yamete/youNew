<?php
class AppealController extends ControllerBase{
    public function indexAction($pageNum){
        if(!$this->session->has('uname')){//判断管理员是否登录，没登录就定位到登录页面
            $this->response->redirect('/login');
        }
        $currentPage=(int)empty($pageNum)?1:$pageNum;
        $config=Config::findFirst();//取出站点配置信息
        $phql="select s.id,s.title,s.content,s.image,s.create_time,s.handle_time,s.handle_status,s.sn,s.isfreeze,u.account from Service s,User u where u.id=s.user_id and s.topic=3";
        $recordList=$this->modelsManager->executeQuery($phql);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $recordList,
                "limit"=> $config->record_num,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        if($page->last<=$config->page_offset*2+1){  //若要总页数小于或等于要显示的页码数，让起始页码等于1，结束页码等于总页数
            $startnum=1;
            $lastnum=$page->last;
        }else{  //若总页数大于要显示的页码数
            if($currentPage-$config->page_offset<=1){   //若当前页码减去偏移量小于或等于1，让起始页码等于1，结束页码等于要显示的页码数
                $startnum=1;
                $lastnum=$config->page_offset*2+1;
            }else{  //若当面页码减去偏移量大于1
                $startnum=$currentPage-$config->page_offset <= 1 ? 1 : $currentPage-$config->page_offset;   //若当前页码减去偏移量小于或等于1，则让起始页码等于1，否则就让起始页码等于当前页码减去偏移量
                $startnum=$currentPage+$config->page_offset >= $page->last ? $page->last-$config->page_offset*2 : $currentPage-$config->page_offset; //若当前页码加上偏移量大于或等于总页数，则让起始页码等于总页数减去偏移量的2倍，否则就让起始页码等于当前页码减去偏移量
                $lastnum=$currentPage+$config->page_offset >= $page->last ? $page->last:$currentPage+$config->page_offset;  //若当前页码加上偏移量大于或等于总页数，则让结束页码等于总页数，否则就让结束页码等于当前页码加上偏移量
            }
        }
        $this->view->setVars(array(
            'lastnum'=>$lastnum,
            'startnum'=>$startnum,
            'currentPage'=>$currentPage,
            'page'=>$page,
            'operation'=>'appeallist'
        ));
    }
    public function editAction($id){
        $record=Service::findFirst($id);
        $this->view->setVar('record',$record);
    }
    public function doeditAction(){
        $data=$this->request->getPost();
        $this->db->begin();
        $record=Service::findFirst($data['id']);
        unset($data['id']);
        if($data['handle_status']==2||$data['handle_status']==3){
            $data['handle_time']=time();
        }
        if($data['handle_status']==3||$data['isfreeze']==2){
            $user=User::findFirst($record->user_id);
            if(!$user->save(array("isfreeze"=>1))){
                $this->db->rollback();
                return "<script>alert('操作失败，请重试！');history.go(-1);</script>";
            }
        }
        if(!$record->save($data)){
            $this->db->rollback();
            return "<script>alert('操作失败，请重试！');history.go(-1);</script>";
        }
        $this->db->commit();
        return "<script>alert('操作成功！');location='/appeal/index';</script>";
    }
    public function delAction($id){
        $record=Service::findFirst($id);
        if($record->delete()){
            return "<script>alert('删除成功！');location='/appeal/index';</script>";
        }else{
            return "<script>alert('删除失败，请重试！');location='/appeal/index';</script>";
        }
    }
}
?>