<?php
use Phalcon\Mvc\Controller,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AdministratorController extends Controller{
    public function indexAction(){
        if(!$this->session->has('uname')){//判断管理员是否登录，没登录就定位到登录页面
            $this->response->redirect('/login');
        }
        $pageOffset=5;//页码偏移量
        $limit=3;//每页显示记录数
        $currentPage=(int)@$_GET["page"]<1?1:@$_GET["page"];
        $timeArr=array();
        $phql="select a.* from Administrator a";
        $recordList=$this->modelsManager->executeQuery($phql);
        $paginator=new PaginatorModel(
            array(
                "data"  => $recordList,
                "limit" => $limit,
                "page"  => $currentPage
            )
        );
        $page=$paginator->getPaginate();
        foreach($page->items as $item){
            $timeArr[$item->id]=date("Y-m-d H:i",$item->create_time);
        }
        if($page->last<=$pageOffset*2+1){  //若要总页数小于或等于要显示的页码数，让起始页码等于1，结束页码等于总页数
            $startnum=1;
            $lastnum=$page->last;
        }else{  //若总页数大于要显示的页码数
            if($currentPage-$pageOffset<=1){   //若当前页码减去偏移量小于或等于1，让起始页码等于1，结束页码等于要显示的页码数
                $startnum=1;
                $lastnum=$pageOffset*2+1;
            }else{  //若当面页码减去偏移量大于1
                $startnum=$currentPage-$pageOffset <= 1 ? 1 : $currentPage-$pageOffset;   //若当前页码减去偏移量小于或等于1，则让起始页码等于1，否则就让起始页码等于当前页码减去偏移量
                $startnum=$currentPage+$pageOffset >= $page->last ? $page->last-$pageOffset*2 : $currentPage-$pageOffset; //若当前页码加上偏移量大于或等于总页数，则让起始页码等于总页数减去偏移量的2倍，否则就让起始页码等于当前页码减去偏移量
                $lastnum=$currentPage+$pageOffset >= $page->last ? $page->last:$currentPage+$pageOffset;  //若当前页码加上偏移量大于或等于总页数，则让结束页码等于总页数，否则就让结束页码等于当前页码加上偏移量
            }
        }
        $this->view->setVars(array(
            'page'=>$page,
            'timeArr'=>$timeArr,
            'startNum'=>$startnum,
            'lastNum'=>$lastnum,
            'currentPage'=>$currentPage,
            'cat1'=>'admin',
            'cat2'=>'adminList'
        ));
    }
    public function addAction(){
        if(!$this->session->has('uname')){//判断管理员是否登录，没登录就定位到登录页面
            $this->response->redirect('/login');
        }
        $this->view->setVars(array(
            'cat1'=>'admin',
            'cat2'=>'adminAdd'
        ));
    }
    public function doaddAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                $data['time']=time();
                $data['password']=sha1($data['password']);
                $phql="insert into Administrator "
                    ."(name,password,create_time) values "
                    ."(:name:,:password:,:time:)";
                $result=$this->modelsManager->executeQuery($phql,$data);
                if($result->success()){
                    echo json_encode(array("status"=>1));
                    exit;
                }else{
                    echo json_encode(array("status"=>0));
                    exit;
                }
            }
        }
    }
    public function lookAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data = $this->request->getPost();
                $phql="select a.* from Administrator a where a.id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                $str="<table class='table'>";
                foreach($record as $r){
                    $str.="<tr><td>管理员名：</td><td>".$r->name."</td></tr>";
                    $str.="<tr><td>创建时间：</td><td>".date("Y-m-d H:i",$r->create_time)."</td></tr>";
                }
                $str.="</table>";
                echo $str;
                exit;
            }
        }
    }
    public function editAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data=$this->request->getPost();
                $phql="select a.* from Administrator a where a.id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                foreach($record as $r){
                    $str.="<tr>";
                    $str.="<td>管理员名：</td>";
                    $str.="<td><input type='text' class='form-control' name='name' value='".$r->name."'></td>";
                    $str.="</tr>";

                    $str.="<tr>";
                    $str.="<td>管理员密码：</td>";
                    $str.="<td><input type='text' class='form-control' name='password' placeholder='不修改密码请留空'></td>";
                    $str.="</tr>";

                    $str.="<tr>";
                    $str.="<td>创建时间：</td>";
                    $str.="<td>".date("Y-m-d H:i",$r->create_time)."</td>";
                    $str.="</tr>";
                }
                $str.="<input type='hidden' name='id' value='".$data['id']."'>";
                echo $str;
                exit;
            }
        }
    }
    public function doeditAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                if(empty($data['password'])){
                    $phql="update Administrator set name=:name: where id=:id:";
                    unset($data['password']);
                }else{
                    $data['password']=sha1($data['password']);
                    $phql="update Administrator set name=:name:,password=:password: where id=:id:";
                }
                $this->modelsManager->executeQuery($phql,$data);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
    public function delAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()) {
                $data = $this->request->getPost();
                $str = rtrim($data['ids'], ',');
                $phql = "delete from Administrator where id in (" . $str . ")";
                $this->modelsManager->executeQuery($phql);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
}