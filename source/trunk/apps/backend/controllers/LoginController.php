<?php
    class LoginController extends \Phalcon\Mvc\Controller{
        public function indexAction(){

        }
        public function dologinAction(){
            if($this->request->isPost()){
                $data=$this->request->getPost();
                $name=$data['name'];
                $pwd=sha1($data['pwd']);
                $code=strtolower($data['code']);
                $sessioncode=strtolower($this->session->get('code'));
                if($code!=$sessioncode){
                    echo "<script>alert('验证码输入有误！');location='/login/index';</script>";
                    exit;
                }
                $conditions="name=?1 and password=?2";
                $parameters=array(1=>$name,2=>$pwd);
                $admin=Administrator::findFirst(array(
                    $conditions,
                    "bind"=>$parameters
                ));
                if(empty($admin->id)){
                    echo "<script>alert('用户名与密码不匹配！');location='/login/index';</script>";
                    exit;
                }
                $this->session->set('uid',$admin->id);
                $this->session->set('uname',$admin->name);
                header('Location:/');
            }
        }
    }
?>