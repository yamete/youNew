<?php
class ComplainController extends ControllerBase{
    public function indexAction($pageNum){
        if(!$this->session->has('uname')){//判断管理员是否登录，没登录就定位到登录页面
            $this->response->redirect('/login');
        }
        $currentPage=(int)empty($pageNum)?1:$pageNum;
        $config=Config::findFirst();//取出站点配置信息
        $phql="select s.id,s.user_account,s.content,s.type,s.create_time,s.handle_time,s.handle_status,s.sn,u.account from Service s,User u where u.id=s.user_id and s.topic=1";
        $complain=$this->modelsManager->executeQuery($phql);
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $complain,
                "limit"=> $config->record_num,
                "page" => $currentPage
            )
        );
        $page = $paginator->getPaginate();
        if($page->last<=$config->page_offset*2+1){  //若要总页数小于或等于要显示的页码数，让起始页码等于1，结束页码等于总页数
            $startnum=1;
            $lastnum=$page->last;
        }else{  //若总页数大于要显示的页码数
            if($currentPage-$config->page_offset<=1){   //若当前页码减去偏移量小于或等于1，让起始页码等于1，结束页码等于要显示的页码数
                $startnum=1;
                $lastnum=$config->page_offset*2+1;
            }else{  //若当面页码减去偏移量大于1
                $startnum=$currentPage-$config->page_offset <= 1 ? 1 : $currentPage-$config->page_offset;   //若当前页码减去偏移量小于或等于1，则让起始页码等于1，否则就让起始页码等于当前页码减去偏移量
                $startnum=$currentPage+$config->page_offset >= $page->last ? $page->last-$config->page_offset*2 : $currentPage-$config->page_offset; //若当前页码加上偏移量大于或等于总页数，则让起始页码等于总页数减去偏移量的2倍，否则就让起始页码等于当前页码减去偏移量
                $lastnum=$currentPage+$config->page_offset >= $page->last ? $page->last:$currentPage+$config->page_offset;  //若当前页码加上偏移量大于或等于总页数，则让结束页码等于总页数，否则就让结束页码等于当前页码加上偏移量
            }
        }
        $this->view->setVars(array(
            'lastnum'=>$lastnum,
            'startnum'=>$startnum,
            'currentPage'=>$currentPage,
            'page'=>$page,
            'operation'=>'complainlist'
        ));
    }
    public function editAction($id){
        $complain=Service::findFirst($id);
        $this->view->setVar('complain',$complain);
    }
    public function doeditAction(){
        $data=$this->request->getPost();
        $this->db->begin();
        $complain=Service::findFirst($data['id']);
        unset($data['id']);
        if($data['handle_status']==2||$data['handle_status']==3){
            $data['handle_time']=time();
        }
        if($data['handle_status']==3&&$data['low']==2){
            $condition="account='".$complain->user_account."'";
            $user=User::findFirst(array($condition));
            $low=$user->low_evaluate + 1;
            if(!$user->save(array("low_evaluate"=>$low))){
                $this->db->rollback();
                return "<script>alert('操作失败，请重试！');history.go(-1);</script>";
            }
        }
        if(!$complain->save($data)){
            $this->db->rollback();
            return "<script>alert('操作失败，请重试！');history.go(-1);</script>";
        }
        $this->db->commit();
        return "<script>alert('操作成功！');location='/complain/index';</script>";
    }
    public function delAction($id){
        $complain=Service::findFirst($id);
        if($complain->delete()){
            $this->response->redirect('/complain/index');
        }else{
            echo "抱歉，无法删除！";
        }
    }
}
?>