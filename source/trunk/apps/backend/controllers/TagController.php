<?php
use Phalcon\Mvc\Controller,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class TagController extends Controller{
    public function indexAction(){
        $pageOffset=5;//页码偏移量
        $limit=3;//每页显示记录数
        $currentPage=(int)@$_GET["page"]<1?1:@$_GET["page"];
        $timeArr=array();
        $phql="select t.* from Tag t order by t.tag_id desc";
        $recordList=$this->modelsManager->executeQuery($phql);
        $paginator=new PaginatorModel(
            array(
                "data"  => $recordList,
                "limit" => $limit,
                "page"  => $currentPage
            )
        );
        $page=$paginator->getPaginate();
        if($page->last<=$pageOffset*2+1){  //若要总页数小于或等于要显示的页码数，让起始页码等于1，结束页码等于总页数
            $startnum=1;
            $lastnum=$page->last;
        }else{  //若总页数大于要显示的页码数
            if($currentPage-$pageOffset<=1){   //若当前页码减去偏移量小于或等于1，让起始页码等于1，结束页码等于要显示的页码数
                $startnum=1;
                $lastnum=$pageOffset*2+1;
            }else{  //若当面页码减去偏移量大于1
                $startnum=$currentPage-$pageOffset <= 1 ? 1 : $currentPage-$pageOffset;   //若当前页码减去偏移量小于或等于1，则让起始页码等于1，否则就让起始页码等于当前页码减去偏移量
                $startnum=$currentPage+$pageOffset >= $page->last ? $page->last-$pageOffset*2 : $currentPage-$pageOffset; //若当前页码加上偏移量大于或等于总页数，则让起始页码等于总页数减去偏移量的2倍，否则就让起始页码等于当前页码减去偏移量
                $lastnum=$currentPage+$pageOffset >= $page->last ? $page->last:$currentPage+$pageOffset;  //若当前页码加上偏移量大于或等于总页数，则让结束页码等于总页数，否则就让结束页码等于当前页码加上偏移量
            }
        }
        $this->view->setVars(array(
            'page'=>$page,
            'startNum'=>$startnum,
            'lastNum'=>$lastnum,
            'currentPage'=>$currentPage,
            'cat1'=>'tag',
            'cat2'=>'tagList'
        ));
    }
    public function addAction(){
        $this->view->setVars(array(
            'cat1'=>'tag',
            'cat2'=>'tagAdd'
        ));
    }
    public function doaddAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                $model=new Tag();
                if($model->save($data)){
                    echo json_encode(array("status"=>1));
                    exit;
                }else{
                    echo json_encode(array("status"=>0));
                    exit;
                }
            }
        }
    }
    public function lookAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data = $this->request->getPost();
                $phql="select t.* from Tag t where t.tag_id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                $str="<table class='table'>";
                foreach($record as $r){
                    if($r->tag_type==1){
                        $type='淘宝';
                    }elseif($r->tag_type==2){
                        $type='京东';
                    }
                    $str.="<tr><td>标签名：</td><td>".$r->tag_name."</td></tr>";
                    $str.="<tr><td>标签类型：</td><td>".$type."</td></tr>";
                }
                $str.="</table>";
                echo $str;
                exit;
            }
        }
    }
    public function editAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $str='';
                $data = $this->request->getPost();
                $phql="select t.* from Tag t where t.tag_id=:id:";
                $record=$this->modelsManager->executeQuery($phql,$data);
                foreach($record as $r){
                    $str.="<tr><td>标签名：</td><td><input type='text' class='form-control' name='tag_name' value='".$r->tag_name."'></td></tr>";
                    $str.="<tr><td>标签类型：</td><td>";
                    $str.="<div class='icheck'><div class='square-blue single-row'>";
                    if($r->tag_type==1){
                        $str.="<div class='radio'><input type='radio'  name='tag_type' value='1' checked><label>淘宝</label></div>";
                        $str.="<div class='radio'><input type='radio'  name='tag_type' value='2'><label>京东</label></div>";
                    }elseif($r->tag_type==2){
                        $str.="<div class='radio'><input type='radio'  name='tag_type' value='1'><label>淘宝</label></div>";
                        $str.="<div class='radio'><input type='radio'  name='tag_type' value='2' checked><label>京东</label></div>";
                    }
                    $str.="</div></div></td></tr>";
                }
                $str.="<input type='hidden' name='id' value='".$data['id']."'>";
                echo $str;
                exit;
            }
        }
    }
    public function doeditAction(){
        if($this->request->isPost()) {
            if ($this->request->isAjax()) {
                $data=$this->request->getPost();
                $record=Tag::findFirst($data['id']);
                unset($data['id']);
                $record->save($data);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
    public function delAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()) {
                $data = $this->request->getPost();
                $str = rtrim($data['ids'], ',');
                $phql = "delete from Tag where tag_id in (" . $str . ")";
                $this->modelsManager->executeQuery($phql);
                if ($this->db->affectedRows()) {
                    echo json_encode(array("status" => 1));
                    exit;
                } else {
                    echo json_encode(array("status" => 0));
                    exit;
                }
            }
        }
    }
}