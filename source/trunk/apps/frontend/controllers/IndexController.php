<?php

use \Phalcon\Text;

/**
 * 首页 - 栏目
 */
class IndexController extends ControllerBase{
	public function initialize(){
		header("Content-type:text/html;charset=utf-8");
	}
    //首页
    public function indexAction() {
        $this->view->setVars(array(
            'navFirst'=>1,
            'isLogin'=>$this->isLogin()
        ));
    }

   //注册页
    public function registerAction() {
    	$this->layout='';
        $promoteCode=@$_GET['t'];
		if(!empty($promoteCode)){
			$condition="user_promote_code=?1";
	        $param=array(1=>$promoteCode);
	        $promoteUser=User::findFirst(array(
	            $condition,
	            "bind"=>$param
	        ));
	        if($promoteUser->user_id>0){
	            $this->cookies->set('master-id', $promoteUser->user_id, time() + 24*3600);
	        }
		}
        
    }
	
	
    /**
     * 忘记密码
     */
    public function lostpasswordAction() {

    }
    public function confirmquestionAction(){
        $account=$this->request->getPost('account');
        $condition="account=?1";
        $params=array(1=>$account);
        $user=User::findFirst(array($condition,"bind"=>$params));
        if($user->id>0){
            $this->view->setVars(array(
                'user'=>$user,
                'account'=>$account
            ));
        }else{
            echo "<script>alert('此用户不存在！');location='/';</script>";
            exit;
        }
    }
    public function findpasswordAction(){
        $data=$this->request->getPost();
        $condition="account=?1 and answer1=?2 and answer2=?3";
        $params=array(1=>$data['account'],2=>$data['answer1'],3=>$data['answer2']);
        $user=User::findFirst(array($condition,"bind"=>$params));
        if($user->id){
            $this->view->setVars(array(
                'account'=>$data['account'],
                'answer1'=>$data['answer1'],
                'answer2'=>$data['answer2']
            ));
        }else{
            echo "<script>alert('密保答案输入有误！');location='/';</script>";
            exit;
        }
    }
    public function changepasswordAction(){
        $data=$this->request->getPost();
        if(!preg_match('/^[\w]{6,32}$/',$data['password'])){
            echo "<script>alert('密码格式有误，请重新输入！');history.go(-1);</script>";
            exit;
        }
        if($data['password']!=$data['repassword']){
            echo "<script>alert('两次密码输入不一致，请重新输入！');history.go(-1);</script>";
            exit;
        }
        $data['password']=sha1($data['password']);
        $condition="account=?1 and answer1=?2 and answer2=?3";
        $params=array(1=>$data['account'],2=>$data['answer1'],3=>$data['answer2']);
        $user=User::findFirst(array($condition,"bind"=>$params));
        if($user->id){
            if($user->save(array("password"=>$data['password']))){
                echo "<script>alert('密码修改成功，请登录！');location='/';</script>";
                exit;
            }else{
                echo "<script>alert('密码修改失败，请稍候重试！');history.go(-1);</script>";
                exit;
            }
        }else{
            echo "<script>alert('操作出错，请稍候重试！');history.go(-1);</script>";
            exit;
        }
    }
    //退出登录
    public function logoutAction(){
        $this->session->destroy();
        $this->response->redirect("/");
    }

}
