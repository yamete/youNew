<?php
    class PromotionController extends ControllerBase{
        protected $layout='sidebarp';
        //推荐说明
        public function indexAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            $this->view->setVars(array(
                'operation'=>'explain'
            ));
        }
        //奖励排行
        public function rewardrankAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            $phql="select p.coin,u.account from Promote p,User u where u.id=p.user_id order by p.coin desc";
            $promote=$this->modelsManager->executeQuery($phql);
            $this->view->setVars(array(
                'promote'=>$promote,
                'operation'=>'rewardrank'
            ));
        }
        //我要推荐
        public function promoteAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            $config=Config::findFirst();
            $condition="id=".$this->session->get('uid');
            $user=User::findFirst(array($condition));
            $link=$config->domain."?id=".$user->promote;
            $this->view->setVars(array(
                'link'=>$link,
                'operation'=>'promote'
            ));
        }
        //推荐记录
        public function promoterecordAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            //我
            $condition="id=".$this->session->get('uid');
            $user=User::findFirst(array($condition));
            //我推荐的人
            $condition="master_id=".$this->session->get('uid');
            $recommendUser=User::find(array($condition));
            $personNum=$recommendUser->count();
            //我的推荐人
            $condition="id=".$user->master_id;
            $promoteUser=User::findFirst(array($condition));
            $this->view->setVars(array(
                'user'=>$user,
                'puser'=>$promoteUser,
                'ruser'=>$recommendUser,
                'rcount'=>$personNum,
                'operation'=>'promoterecord'
            ));
        }
    }