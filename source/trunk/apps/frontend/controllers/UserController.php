<?php
    class UserController extends ControllerBase{
        //个人中心
        public function indexAction(){
            $isLogin=$this->isLogin();
            if($isLogin){
                $moneyDetailList=MoneyDetail::find(array("order"=>"change_time desc"));
                $this->view->setVars(array(
                    'navFirst'=>4,
                    'navSecond'=>1,
                    'isLogin'=>$isLogin,
                    'moneyDetailList'=>$moneyDetailList
                ));
            }else{
                $this->response->redirect("/");
            }
        }
        //个人中心-我的资金
        public function mymoneyAction(){
            $isLogin=$this->isLogin();
            if($isLogin){
                $this->view->setVars(array(
                    'navFirst'=>4,
                    'navSecond'=>4,
                    'isLogin'=>$isLogin
                ));
            }else{
                $this->response->redirect("/");
            }
        }
    }
?>