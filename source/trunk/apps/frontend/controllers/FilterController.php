<?php
class FilterController extends \Phalcon\Mvc\Controller{
    public function initialize(){
        header("Content-type:text/html;charset=utf-8");
    }
    public function indexAction(){
        if($this->request->isPost()){
            if($this->request->isAjax()){
                $data=$this->request->getPost();
                //echo json_encode($data);exit;
                if(count($data['entrance'])>0){
                    if($this->session->has('taskEntrance')){
                        $this->session->remove('taskEntrance');
                    }
                    $this->session->set('taskEntrance',$data['entrance']);
                }else{
                    $this->session->remove('taskEntrance');
                }
                if(count($data['pay_type'])>0){
                    if($this->session->has('payType')){
                        $this->session->remove('payType');
                    }
                    $this->session->set('payType',$data['pay_type']);
                }else{
                    $this->session->remove('payType');
                }
                if(count($data['bg_id'])>0){
                    if($this->session->has('buyerLevel')){
                        $this->session->remove('buyerLevel');
                    }
                    $this->session->set('buyerLevel',$data['bg_id']);
                }else{
                    $this->session->remove('buyerLevel');
                }
                if(!empty($data['total_price'])&&$data['total_price']!='all'){
                    if($this->session->has('taskPrice')){
                        $this->session->remove('taskPrice');
                    }
                    $this->session->set('taskPrice',$data['total_price']);
                }else{
                    $this->session->remove('taskPrice');
                }
                echo json_encode(array("status"=>1));exit;
            }else{
                $this->response->redirect("/");
            }
        }else{
            $this->response->redirect("/");
        }
    }
}
?>