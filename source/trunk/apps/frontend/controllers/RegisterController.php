<?php
class RegisterController extends ControllerBase{
    protected $layout='';
    //注册实名认证
    public function nextregisteAction(){
        $this->layout='default';
        $data=$this->request->getPost();
        if($data['question1']==0||$data['question2']==0){
            echo "<script>alert('请选择密保问题！');location='/index/reg';</script>";
            exit;
        }
        if($data['question1']!=0){
            $data['answer1']=preg_replace('/\s/','',$data['answer1']);
            if(empty($data['answer1'])){
                echo "<script>alert('请输入第一问题答案！');location='/index/reg';</script>";
                exit;
            }
        }
        if($data['question2']!=0){
            $data['answer2']=preg_replace('/\s/','',$data['answer2']);
            if(empty($data['answer2'])){
                echo "<script>alert('请输入第二问题答案！');location='/index/reg';</script>";
                exit;
            }
        }
        $data['password']=sha1($data['password']);
        $province=China::find(array("Pid=0 and Id!=0"));
        $this->view->setVars(array(
            'data'=>$data,
            'province'=>$province
        ));
    }
    //ajax无刷上传图片
    public function uploadthumbAction(){
        $this->layout='';
        if($this->request->hasFiles()){
            $datestr=date('Ymd',time());
            $path='../uploads/'.$datestr.'/';
            if(!file_exists($path)){
                mkdir($path);
            }
            $config=Config::findFirst();
            foreach ($this->request->getUploadedFiles() as $file){
                if($file->getName()){
                    $type=strtolower($file->getExtension());
                    $size=$file->getSize();
                    $typearr=array('jpg','png','gif');
                    if(!in_array($type,$typearr)){//若图片不是jpg，png或gif格式，则禁止上传
                        return json_encode(array('status'=>0,'info'=>'无法上传此类型的图片！'));
                    }
                    if($size>2*1024*1024){//若图片大小大于2MB，禁止上传
                        return json_encode(array('status'=>0,'info'=>'您上传的图片过大！'));
                    }
                    $imgUrl=date('YmdHis',time()).uniqid().'.'.$type;
                    $file->moveTo($path.$imgUrl);
                    $imgName=$config->img_domain.$datestr.'/'.$imgUrl;
                }
            }
            echo json_encode(array('status'=>1,'info'=>$imgName));
            exit;
        }else{
            echo json_encode(array('status'=>0,'info'=>'您还没有选择图片！'));
            exit;
        }
    }
    //根据传入的省id获取市
    public function getcityAction(){
        $this->layout='';
        if($this->request->isPost()){
            $str="<option value='0' selected='selected'>--请选择--</option>";
            $id=$this->request->getPost('id');
            if($id){
                if($id==110000||$id==120000||$id==310000||$id==500000){
                    $condition="Id=".$id;
                }else{
                    $condition="Pid=".$id;
                }
                $city=China::find(array($condition));
                foreach($city as $c){
                    $str.="<option value='".$c->Id."'>".$c->Name."</option>";
                }
                echo $str;
                exit;
            }else{
                echo '';
                exit;
            }
        }
    }
    //根据传入的市id获取区域
    public function getareaAction(){
        $this->layout='';
        if($this->request->isPost()){
            $str="<option value='0' selected='selected'>--请选择--</option>";
            $id=$this->request->getPost('id');
            if($id){
                $condition="Pid=".$id;
                $area=China::find(array($condition));
                if($area->count()>0){
                    foreach($area as $a){
                        $str.="<option value='".$a->Id."'>".$a->Name."</option>";
                    }
                }else{
                    $area=China::findFirst($id);
                    $str="<option value='".$area->Id."'>".$area->Name."</option>";
                }
                echo $str;
                exit;
            }else{
                echo '';
                exit;
            }
        }
    }
    //接收用户输入，进行会员注册
    public function doregisteAction(){
        if($this->request->isPost()){
            $data=$this->request->getPost();
            $data['alipay']=preg_replace('/\s/','',$data['alipay']);
            $data['tenpay']=preg_replace('/\s/','',$data['tenpay']);
            if(!preg_match('/^[\x{4e00}-\x{9fa5}]{1,10}$/u',$data['real_name'])){
                echo "<script>alert('请输入正确的姓名！');history.go(-1);</script>";
                exit;
            }
            if(!$this->check_identity($data['idcard_number'])){
                echo "<script>alert('请输入正确的身份证号码！');history.go(-1);</script>";
                exit;
            }
            if(!preg_match('/^[\x{4e00}-\x{9fa5}0-9a-zA-Z]{3,50}$/u',$data['address'])){
                echo "<script>alert('请输入正确的联系地址！');history.go(-1);</script>";
                exit;
            }

            if(empty($data['alipay'])||empty($data['tenpay'])||empty($data['shop_pic'])||empty($data['idcard_back'])||empty($data['idcard_front'])||empty($data['gesture'])||empty($data['idcard_gesture'])||$data['province']==0||$data['city']==0||$data['area']==0){
                echo "<script>alert('请将信息填写完整！');history.go(-1);</script>";
                exit;
            }
            $data['reg_time']=time();
            $data['promote']=substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 6);
            $this->db->begin();
            $user=new User();
            //注册
            if(!$user->save($data)){
                $this->db->rollback();
                echo "<script>alert('注册失败，请稍候重试！');history.go(-1);</script>";
                exit;
            }
            if($data['master_id']>0){
                //当会员注册成功后，若有推荐人，则对推荐人进行金币奖励
                $condition="id=".$data['master_id'];
                $promoteUser=User::findFirst(array($condition));
                $gcoin=$promoteUser->gcoin + 50;
                $regCoin=$promoteUser->reg_coin + 50;
                if(!$promoteUser->save(array("gcoin"=>$gcoin,"reg_coin"=>$regCoin))){
                    $this->db->rollback();
                    echo "<script>alert('注册失败，请稍候重试！');history.go(-1);</script>";
                    exit;
                }
                //并将奖励记录下来
                $condition="user_id=".$data['master_id'];
                $promote=Promote::findFirst(array($condition));
                if($promote->id){
                    $coin=$promote->coin + 50;
                    if(!$promote->save(array("coin"=>$coin))){
                        $this->db->rollback();
                        echo "<script>alert('注册失败，请稍候重试！');history.go(-1);</script>";
                        exit;
                    }
                }else{
                    $promote=new Promote();
                    if(!$promote->save(array("user_id"=>$data['master_id'],"coin"=>50))){
                        $this->db->rollback();
                        echo "<script>alert('注册失败，请稍候重试！');history.go(-1);</script>";
                        exit;
                    }
                }
            }
            //记录IP以及登录时间
            $datestr=time();
            $ip=$_SERVER['REMOTE_ADDR'];
            $user=User::findFirst(array("order"=>"id desc"));
            if(!$user->save(array('login_time'=>$datestr,'ip'=>$ip))){
                $this->db->rollback();
                echo "<script>alert('注册失败，请稍候重试！');history.go(-1);</script>";
                exit;
            }
            $this->session->set('uid',$user->id);
            $this->session->set('uname',$user->account);
            $this->session->set('utype',$user->type);
            $this->session->set('uidentity',$user->identity);
            $this->session->set('ip',$ip);
            $this->session->set('login_time',$datestr);
            $this->db->commit();
            echo "<script>alert('注册成功！');location='/';</script>";
            exit;
        }
    }
}
?>