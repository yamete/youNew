<?php
    class ServiceController extends ControllerBase{
        protected $layout='sidebars';
        //投诉中心
        public function indexAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            if($this->request->isPost()){
                $data=$this->request->getPost();
                $condition="account='".$data['user_account']."'";
                $user=User::findFirst(array($condition));
                if($user->id>0){
                    $data['topic']=1;
                    $data['user_id']=$this->session->get('uid');
                    $data['create_time']=time();
                    $service=new Service();
                    if($service->save($data)){
                        echo "<script>alert('提交成功，请耐心等待处理！');location='/service/index';</script>";
                        exit;
                    }else{
                        echo "<script>alert('提交失败，请稍候重试！');location='/service/index';</script>";
                        exit;
                    }
                }else{
                    echo "<script>alert('提交失败，您投诉的会员不存在！');location='/service/index';</script>";
                    exit;
                }
            }
            $this->view->setVars(array(
                'operation'=>'complain'
            ));
        }
        //网站建议
        public function suggestionAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            if($this->request->isPost()){
                $data=$this->request->getPost();
                $data['title']=trim($data['title']);
                $data['content']=trim($data['content']);
                if(empty($data['title'])||empty($data['content'])){
                    echo "<script>alert('提交失败，标题或内容不能为空！');location='/service/suggestion';</script>";
                    exit;
                }
                $data['topic']=2;
                $data['user_id']=$this->session->get('uid');
                $data['create_time']=time();
                $service=new Service();
                if($service->save($data)){
                    echo "<script>alert('提交成功，请耐心等待处理！');location='/service/suggestion';</script>";
                    exit;
                }else{
                    echo "<script>alert('提交失败，请稍候重试！');location='/service/suggestion';</script>";
                    exit;
                }
            }
            $this->view->setVars(array(
                'operation'=>'suggestion'
            ));
        }
        //在线客服
        public function onlineAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            $this->view->setVars(array(
                'operation'=>'online'
            ));
        }
        //解冻申诉
        public function appealAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            if($this->request->isPost()){
                $data=$this->request->getPost();
                $data['title']=trim($data['title']);
                $data['content']=trim($data['content']);
                if(empty($data['title'])||empty($data['content'])){
                    echo "<script>alert('提交失败，标题或内容不能为空！');location='/service/appeal';</script>";
                    exit;
                }
                $data['topic']=3;
                $data['user_id']=$this->session->get('uid');
                $data['create_time']=time();
                if($this->request->hasFiles()){
                    $config=Config::findFirst();//取出站点配置信息
                    $datestr=date('Ymd',time());
                    $path='../uploads/'.$datestr.'/';
                    if(!file_exists($path)){
                        mkdir($path);
                    }
                    foreach ($this->request->getUploadedFiles() as $file) {
                        $imgUrl=date('YmdHis',time()).uniqid().'.'.$file->getExtension();
                        $file->moveTo($path.$imgUrl);
                        $data['image']=$config->img_domain.$datestr.'/'.$imgUrl;
                    }
                }
                $service=new Service();
                if($service->save($data)){
                    echo "<script>alert('提交成功，请耐心等待处理！');location='/service/appeal';</script>";
                    exit;
                }else{
                    echo "<script>alert('提交失败，请稍候重试！');location='/service/appeal';</script>";
                    exit;
                }
            }
            $this->view->setVars(array(
                'operation'=>'appeal'
            ));
        }
        //处理记录
        public function handleAction(){
            if(!$this->session->has('uid')){//判断会员是否登录，没登录就定位到登录页面
                header('Location: /');
            }
            $condition="user_id=".$this->session->get('uid');
            $service=Service::find(array($condition));
            $this->view->setVars(array(
                'service'=>$service,
                'operation'=>'handle'
            ));
        }
    }
