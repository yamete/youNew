<?php
    class ArticleController extends ControllerBase{
        protected $layout='article';
        public function showAction($id){
            $article=Article::findFirst($id);
            $this->view->setVars(array(
                'article'=>$article
            ));
        }
        public function listAction($id){
            $type=ArticleType::findFirst($id);
            $condition="type=?1";
            $param=array(1=>$id);
            $articleList=Article::find(array(
                $condition,
                'bind'=>$param
            ));
            $this->view->setVars(array(
                'type'=>$type,
                'articleList'=>$articleList
            ));
        }
    }
?>